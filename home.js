/**
 * Created by Administrator on 2014/4/29.
 */
var gui = require('nw.gui');
var win = gui.Window.get();
var user  = global.homedata.user ;
var socket = global.homedata.socket;
var reqchatwin = null;
var prvwin = null;
var pubwin = null;
var hiddened = false;
onload = function(){
    $("#userinfo").prepend("<li>"+user.localName+"</li>");
    $("#userinfo").prepend("<li>"+user.groupName+"</li>");
    $("#userid").append(user.username);
}

var tray = null;
function hideTray() {
    win.show();
    if (tray != null)tray.remove();
    tray = null;
    hiddened = false;
}

function showTray(){
    tray = new gui.Tray({ icon: 'images/tray.png' });
    tray.on('click', function () {
        hideTray();
    });
    hiddened = true;
}

win.on('minimize', function () {
    // Hide window
    this.hide();
    // Show tray
    showTray();
});



socket.on('welcome',function(data){
    appendMessage('msginfos',data);
});


socket.on('logoutEvent',function(data){
   if(win) win.close(true);
});


socket.on('sayHiEvent', function (data) {
    logout(event);
});
socket.on('reqchat',function(data){

   // console.log(data);
    openreqchatwin({sender:data.sender,receiver:data.receiver,socket:socket});
   // console.log(reqchatwin);
});
socket.on('reqaccept',function(data){

    console.log(data);
    openforprivate({sender:data.sender,receiver:data.receiver,socket:socket});
    console.log(prvwin);
});
socket.on('reqreject',function(data){
    var rec      = data.receiver.name;
    var reclocal = data.receiver.localName;
    var gname    = data.receiver.group.value;
    var msg=gname+reclocal+'['+rec+']拒绝了对话请求';
    checkhitwin({message:sytemtip(msg),socket:socket});
});

socket.on('reqchatfailure',function(data){
    var msg = '请求和'+data.receiver.localName+"对话失败,对方不在线.";
    checkhitwin({message:sytemtip(msg),socket:socket});
});



function openreqchatwin(reqdata){
    global.reqchatdata = reqdata;
    var mw = 250, mh = 130;
    if (reqchatwin == null) {
        reqchatwin = gui.Window.open(
            "reqchat.html",
            {
                frame: false,
                toolbar: true,
                show: false,
                show_in_taskbar: false,
                'always-on-top': true,
                width: mw,
                height: mh
            }

        );
        var sw = window.screen.availWidth;
        var sh = window.screen.availHeight;
        reqchatwin.moveTo(sw - mw, sh - mh);
    }


    reqchatwin.reloadIgnoringCache();
    reqchatwin.show();

}


function openforprivate(data){
    global.prvdata = data;
    if (prvwin == null) {
        prvwin = gui.Window.open(
            "pvtchat.html",
            {
                frame: false,
                toolbar: true,
                show: false,
                show_in_taskbar: true,
                'always-on-top': true,
                "resizable":false,
                height:580
            }

        );
    }

    console.log(global.prvdata);

    prvwin.show();
}



function openpublicroom(data){
    global.pubdata = data;
    if (pubwin == null) {
        pubwin = gui.Window.open(
            "publicroom.html",
            {
                frame: false,
                toolbar: true,
                show: false,
                show_in_taskbar: true,
                'always-on-top': true,
                "resizable":false,
                height:580
            }

        );
    }

    pubwin.show();
}

function userofchat(receiver) {

    socket.emit('reqchat',{
        sender:{
            name:user.name
        }
        , receiver:{
            name:receiver.name
        }
    });
    var msg = sytemtip('请求和'+receiver.localName+"对话,正在等候对方响应,请等待..");
    checkhitwin({message:msg,socket:socket});

}


function closeme(event){
    minisize(event);
}
function minisize(event) {
    win.minimize();
}

function gotoProom(){
    openpublicroom({socket:socket,user:user});
}

function logout(event) {
    socket.emit('logoutEvent', "i 'm logout");
    win.close(true);
}



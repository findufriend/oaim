/**
 * Created by Administrator on 14-2-27.
 */

var wgui = require('nw.gui');
var hitwin=null;
function checkhitwin(reqdata){
    global.hitdata = reqdata;
    var mw = 250, mh = 130;
    if (hitwin == null) {
        hitwin = wgui.Window.open(
               "messagehit.html",
            {
                frame: false,
                toolbar: true,
                show: false,
                show_in_taskbar: false,
                'always-on-top': true,
                width: mw,
                height: mh
            }

        );
        var sw = window.screen.availWidth;
        var sh = window.screen.availHeight;
        hitwin.moveTo(sw - mw, sh - mh);
    }

    hitwin.reloadIgnoringCache();
    hitwin.show();
}


function saysAutoScroll(id) {
    var sdiv = $('#' + id);
    sdiv.scrollTop(sdiv[0].scrollHeight);
}

function appendMessage(pid,data) {
    var chatsays = $("#"+pid);
    var line = $("<li></li>").html(data.content);
    chatsays.append(line);
    saysAutoScroll(pid);
}


function getCkData(id){
    var msg = CKEDITOR.instances[id].getData();
    return msg;
}

function render(ckid) {

    CKEDITOR.replace(ckid);

    safecleanckeditor(ckid);
    CKEDITOR.on('instanceReady', function (e) {
        if (e.editor.document.$.addEventListener)
            e.editor.document.$.addEventListener('keydown', ckekeydown, false);
        else if (e.editor.document.$.attachEvent)
            e.editor.document.$.attachEvent('onkeyup', function (e) {
                ckekeydown(e)
            });
    });
}

function ckekeydown(e) {
    if (e.keyCode == 13 && e.ctrlKey) {
        //alert('按下了ctrl+Enter');
        sendMessage(event);

    }
}

function safecleanckeditor(ckid) {
    CKEDITOR.instances[ckid].setData("", function () {
        if (this.document.$.addEventListener)
            this.document.$.addEventListener('keydown', ckekeydown, false);
        else if (this.document.$.attachEvent)
            this.document.$.attachEvent('onkeyup', function (e) {
                ckekeydown(e)
            });
    });
}


function sytemtip(msg){
    var data = {content:"<code>"+msg+"</code>"};
    return data;
}
/**
 * Created by Foold on 2014-4-30.
 */
var gui = require('nw.gui');
var localwin = gui.Window.get();
var sender=null,receiver=null,socket=null;
socket = global.reqchatdata.socket;
sender = global.reqchatdata.sender;
receiver = global.reqchatdata.receiver;
onload = function(){
    console.log(global.reqchatdata);


    $("#msg").html(sender.localName);

    localwin.show();
}
socket.on('logoutEvent',function(){
    localwin.close(true);
});


function accept(){
    var adata ={
        sender:{name:sender.name},
        receiver:{name:receiver.name}
    };
    socket.emit('reqaccept',adata);
}

function reject(){
    var adata ={
        sender:{name:sender.name},
        receiver:{name:receiver.name}
    };
    socket.emit('reqreject',adata);
    closeme();
}

function closeme(){
    localwin.hide();
}


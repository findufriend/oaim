/**
 * Created by Administrator on 14-2-27.
 */
var gui = require('nw.gui');
var prvwin = gui.Window.get();
var sender= global.prvdata.sender ;
var receiver= global.prvdata.receiver ;
var socket= global.prvdata.socket;
var ckid = 'pvtinput';
onload = function(){
    $("#senderpair")
        .append(sender.group.value)
        .append('-')
        .append(sender.localName);
    $("#receiverpair")
        .append(receiver.group.value)
        .append('-')
        .append(receiver.localName);
    render(ckid);

}

var pid = 'pvtshowcontent';
socket.on('pvtmessage', function (data) {
    // here is your handler on messages from server
    appendMessage(pid,data);

});
socket.on('logoutEvent',function(data){
    prvwin.close(true);
});

function minisize(event) {
    prvwin.minimize();
}

function quitchat(event){
    prvwin.hide();
}

function sendPrivateMessage(ckid,data) {
    var msg = CKEDITOR.instances[ckid].getData();
    if (msg == '' || msg == null) return;

    var pvtmsg = {
        sender:data.sender,
        receiver:data.receiver,
        args:[msg]
    };

    console.log(pvtmsg);
    socket.emit('pvtmessage', pvtmsg);
    safecleanckeditor(ckid);
}

$(function(){
   $("#sendpvt").bind('click',function(event){
       sendPrivateMessage(ckid,{sender:sender,receiver:receiver});
   });
});

